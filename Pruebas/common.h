// Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Funciones del programa principal
int menu();
void altas();
void listados();

//Funciones de altas
void altaDieta();
void altaPaciente();
void altaIngrediente();
void altaProfesional();
void altaPlato();
void addProfPac();
void addDietaPac();
void addControlPac();
void addIngrPlato();

// Funciones de listados.
void listaPac();
void listaProf();
void ListaDiet();
void listaPlat();
void listaIngr();
void listaIngrPlat();

#ifdef _WIN32   // Para windows.

#define CLRSCR() {system("cls");}

#endif

#ifdef linux    // Para linux.

#define CLRSCR() {system("clear");}

#endif
