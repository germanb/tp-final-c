#include "common.h"

int main(){
    menu();
}

int menu() {
    int orden;

    do {    // Loop principal
        CLRSCR();

        // Muestra el menu principal.
        printf("----------------------------------------------------------\n");
        printf("--                    Menu Principal                    --\n");
        printf("----------------------------------------------------------\n");
        printf("\n");
        printf("1. Altas.\n");
        printf("2. Listados.\n\n");
        printf("0. Salir.\n");
        printf("\n");
        printf("Ingrese una opción: ");

        scanf("%d", &orden, stdin);

        // Redirige los menúes.
        switch (orden) {
            case 1: altas();
                break;
            case 2: listados();
                break;
            case 0: break;
            default: printf("No se reconoce la opción. Reingrese...\n\n");
                break;
        }
    } while (orden != 0);

    CLRSCR();   // Limpia la pantalla para finalizar.

    return 0;
}

void altas() {
    int orden;

    do {
        CLRSCR();

        printf("----------------------------------------------------------\n");
        printf("--                    Menu de Altas                     --\n");
        printf("----------------------------------------------------------\n");
        printf("Altas genéricas: \n");
        printf("1. Alta Paciente.\n");
        printf("2. Alta Profesional.\n");
        printf("3. Alta Dieta.\n");
        printf("4. Alta Ingrediente.\n");
        printf("5. Alta Plato\n\n");
        printf("Altas compuestas: \n");
        printf("6. Asignar Profesional a un Paciente.\n");
        printf("7. Agregar una Dieta Nueva a un Paciente.\n");
        printf("8. Agregar un Control Nuevo a un Paciente.\n");
        printf("9. Agregar un Nuevo Ingrediente a un Plato.\n");
        printf("-----------------------\n");
        printf("0. Volver al menú anterior.\n\n");
        printf("Ingrese una opción: ");

        scanf("%d", &orden, stdin);

        // Redirige los menúes.
        switch (orden) {
            case 1: altaPaciente();
                break;
            case 2: altaProfesional();
                break;
            case 3: altaDieta();
                break;
            case 4: altaIngrediente();
                break;
            case 5: altaPlato();
                break;
            case 6: addProfPac();
                break;
            case 7: addDietaPac();
                break;
            case 8: addControlPac();
                break;
            case 9: addIngrPlato();
                break;
            case 0: break;
            default: printf("No se reconoce la opción. Reingrese...\n\n");
                break;
        }

    } while (orden != 0);

    CLRSCR();
}

void listados() {
    int orden;

    do {
        CLRSCR();

        printf("----------------------------------------------------------\n");
        printf("--                   Menu de Listados                   --\n");
        printf("----------------------------------------------------------\n");
        printf("1. Listar Pacientes.\n");
        printf("2. Listar Profesionales.\n");
        printf("3. Listar Dietas.\n");
        printf("4. Listar Platos.\n");
        printf("5. Listar Ingredientes.\n");
        printf("6. Listar Ingredientes de un Plato.\n");
        printf("-----------------------\n");
        printf("0. Volver al menú anterior.\n\n");
        printf("Ingrese una opción: ");

        scanf("%d", &orden, stdin);

        // Redirige los menúes.
        switch (orden) {
            case 1: listaPac();
                break;
            case 2: listaProf();
                break;
            case 3: ListaDiet();
                break;
            case 4: listaPlat();
                break;
            case 5: listaIngr();
                break;
            case 6: listaIngrPlat();
                break;
            case 0: break;
            default: printf("No se reconoce la opción. Reingrese...\n\n");
                break;
        }

    } while (orden != 0);

    CLRSCR();
}

// ******************
// Funciones de altas
void altaDieta(){
}

void altaPaciente(){
}

void altaIngrediente(){
}

void altaProfesional(){
}

void altaPlato(){
}

void addProfPac(){
}

void addDietaPac(){
}

void addControlPac(){
}

void addIngrPlato(){
}

// *********************
// Funciones de listados
void listaPac(){
}

void listaProf(){
}

void ListaDiet(){
}

void listaPlat(){
}

void listaIngr(){
}

void listaIngrPlat(){
}
