#include <stdio.h>
#include <stdlib.h>
#include <libpq-fe.h>
#include <string.h>
#include "config.h"
// incluir fuente de la implementacion de cada objeto del modelo
#include "orm.c"
#include "paciente.c"
#include "dieta.c"
#include "dieta_paciente.c"
#include "plato.c"
#include "plato_dieta.c"
#include "ingrediente.c"
#include "plato_ingrediente.c"
#include "plato_paciente.c"
#include "paciente_control.c"
#include "profesional.c"
#include "paciente_profesional.c"
// Incluye los fuentes de las funcionalidades de alta y listado
#include "altas.c"
#include "listados.c"

int altas();
int listados();

PGconn *conn; //Instancia que permite manipular conexion con el servidor

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
int main() {
    char *port="5432",*servidor="localhost",*base="nutricion", *usuario="postgres", *password="master";

    void *list; //para manejo generico de listado
    int i=0, size=0, j;
    int opcion;

    CLRSCR();

    connectdb(servidor,port,base,usuario,password);

    do {
        printf("*** MENU PRINCIPAL ***\n");
        printf("Ingrese 1 para Altas.\n");
        printf("Ingrese 2 para Listados.\n");
        printf("Ingrese -1 para salir,\n");
        printf(">>> ");
        opcion = getInt();

        if (opcion == 1){
            CLRSCR();
            altas();
            CLRSCR();
        } else if (opcion == 2){
            CLRSCR();
            listados();
            CLRSCR();
        } else if (opcion != -1) {
            CLRSCR();
            printf("Error! Opción inválida. Reingrese...\n\n");
        }

    } while (opcion != -1);

    disconnectdb();

    return 0;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~ //
//          ALTAS           //
// ~~~~~~~~~~~~~~~~~~~~~~~~ //
int altas() {
    int opcion;

    do {
        printf("*** MENU DE ALTAS ***\n");
        printf("Ingrese 1 para dar de alta un paciente.\n");
        printf("Ingrese 2 para dar de alta un profesional.\n");
        printf("Ingrese 3 para dar de alta una dieta.\n");
        printf("Ingrese 4 para dar de alta un ingrediente.\n");
        printf("Ingrese 5 para dar de alta un plato.\n");
        printf("Ingrese 6 para registrar un control a un paciente.\n");
        printf("Ingrese 7 para asignarle un profesional a un paciente.\n");
        printf("Ingrese 8 para asignarle una dieta a un paciente.\n");
        printf("Ingrese 9 para agregarle un ingrediente a un plato.\n");
        printf("Ingrese -1 para volver al menú anterior.\n");
        printf(">>> ");
        opcion = getInt();

        CLRSCR();

        switch (opcion) {
            case 1: CLRSCR();
                    altaPaciente();
                break;
            case 2: CLRSCR();
                    altaProfesional();
                break;
            case 3: CLRSCR();
                    altaDieta();
                break;
            case 4: CLRSCR();
                    altaIngrediente();
                break;
            case 5: CLRSCR();
                    altaPlato();
                break;
            case 6: CLRSCR();
                    altaPacienteControl();
                break;
            case 7: CLRSCR();
                    altaPacienteProfesional();
                break;
            case 8: CLRSCR();
                    altaDietaPaciente();
                break;
            case 9: CLRSCR();
                    altaPlatoIngrediente();
                break;
            case -1: break;

            default: printf("Error! No se reconoce la opción. Reingrese...\n\n");
                break;
        }

        if (opcion != -1){
            printf("\nPresione una tecla para continuar...");
            getchar();
            CLRSCR();
        }

    } while (opcion != -1);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~ //
//         LISTADOS         //
// ~~~~~~~~~~~~~~~~~~~~~~~~ //
int listados() {
    int opcion;

    do {
        printf("*** MENU DE LISTADOS ***\n");
        printf("Ingrese 1 para listado de dietas.\n");
        printf("Ingrese 2 para listado de pacientes.\n");
        printf("Ingrese 3 para listado de ingredientes.\n");
        printf("Ingrese 4 para listado de profesionales.\n");
        printf("Ingrese 5 para listar los platos de una dieta.\n");
        printf("Ingrese 6 para listar los ingredientes de un plato.\n");
        printf("Ingrese -1 para volver al menú anterior.\n");
        printf(">>> ");
        opcion = getInt();

        CLRSCR();

        switch (opcion) {
            case 1: CLRSCR();
                    listarDietas();
                break;
            case 2: CLRSCR();
                    listarPacientes();
                break;
            case 3: CLRSCR();
                    listarIngredientes();
                break;
            case 4: CLRSCR();
                    listarProfesionales();
                break;
            case 5: CLRSCR();
                    listarPlatosDieta();
                break;
            case 6: CLRSCR();
                    listarIngredientesPlato();
                break;
            case -1: break;
            default: printf("Error! No se reconoce la opción. Reingrese...\n\n");
                break;
        }

        if (opcion != -1){
            printf("\n\n\nPresione una tecla para continuar...");
            getchar();
            CLRSCR();
        }

    } while (opcion != -1);
}
