#include "config.h"
#include "listados.h"

// ----------------------------------------------------------------------------
int listarDietas() {
    // Crea el objeto dieta y define su estructura de datos.
    obj_dieta *dieta;
    dieta = dieta_new();

    // Variables de uso general.
    void *lista;
    char *criteria;
    int i, tamanio;
    int resp;

    // Pide el tipo de listado:
    do {
        printf("Ingrese 1 para listar todas las dietas.\n");
        printf("Ingrese 2 para listar las dietas realizadas por un paciente.\n");
        resp = getInt();
        if ((resp != 1)&&(resp != 2))
            printf("Ingreso inválido. Reingrese...\n");
    } while ((resp != 1)&&(resp != 2));

    if (resp == 1){
        // Genera el listado de todas las dietas.
        tamanio = dieta->findAll(dieta, &lista, NULL);
        if (tamanio == 0){
            printf("Tabla vacía! No se listará nada...");
            free(lista);
            return 0;
        }

        resp = 0;
        do {
            printf("Ingrese 1 para listar a pantalla.\n");
            printf("Ingrese 2 para guardar listado en un archivo.\n");
            resp = getInt();
            if ((resp != 1)&&(resp != 2))
            printf("Ingreso inválido. Reingrese...");
        } while ((resp != 1)&&(resp != 2));

        CLRSCR();

        // Guardar en archivo:
        if (resp == 2) {
            // Crea/Abre el archivo.
            FILE *arch;
            arch = fopen("Listado Dietas.txt", "a+");

            fprintf(arch, CABECERA_LISTADO_DIETAS);

            for (i = 0; i < tamanio; i++) {
                // Pide la dieta de posición i..
                dieta = ((obj_dieta**)lista)[i];

                fprintf(arch, "\nDieta N° %d: Código: %d | Nombre: %s | Autor: %s | Descripción: %s",
                i + 1,
                dieta->codigo,
                dieta->nombre,
                dieta->autor,
                dieta->descripcion);
            }
            printf("Finalizado!\n");
            fclose(arch);
            // Listar en pantalla:
        } else {
            printf(CABECERA_LISTADO_DIETAS);

            for (i = 0; i < tamanio; i++) {
                // Pide la dieta de posición i..
                dieta = ((obj_dieta**)lista)[i];

                printf("Dieta N° %d: Código: %d | Nombre: %s | Autor: %s | Descripción: %s\n",
                i + 1,
                dieta->codigo,
                dieta->nombre,
                dieta->autor,
                dieta->descripcion);
            }
        }

        // Libera la memoria usada.
        free(lista);
    } else {
        // Para listar las dietas de un solo paciente.
        listarDietasPaciente();
    }
}
// ----------------------------------------------------------------------------
int listarDietasPaciente() {
    // Crea el objeto paciente.
    obj_paciente *paciente;
    paciente = paciente_new();
    t_data_set_paciente datos_paciente;
    // Crea el objeto dieta.
    obj_dieta *dieta;
    dieta = dieta_new();
    t_data_set_dieta datos_dieta;
    // Crea el objeto dieta-paciente
    obj_dieta_paciente *dietapac;
    dietapac = dieta_paciente_new();
    t_data_set_dieta_paciente datos_dieta_paciente;
    // Otras variables
    void *lista_dieta_paciente;
    int tamanio_lista_dietapac;
    char buffer[MAX];
    char *dni_paciente;
    char *criteria;
    int i, tamanio;
    int tamanio_lista_dieta;
    int existe_paciente;
    int resp;

    // Pide el dni del paciente y verifica que exista.
    printf("\nIngrese el DNI del paciente: ");
    dni_paciente = getLinea();
    strcpy(buffer, dni_paciente);
    datos_paciente.dni = atoi(buffer);
    if (paciente->findbykey(paciente, datos_paciente.dni) != -1) {
        existe_paciente = 1;
    } else {
        existe_paciente = 0;
        printf("El paciente no existe. Reintente...\n");
        free(lista_dieta_paciente);
        return 0;
    }

    /*** Compone la cadena que va a servir de criterio de busqueda ***/
    // Pide memoria para el principio de la criteria.
    criteria = (char *) malloc(sizeof("dni_paciente='"));
    strcpy(criteria, "dni_paciente='");
    // Pide más memoria para la criteria ingresada y la concatena.
    criteria = (char *) realloc(criteria, (sizeof(criteria) + sizeof(dni_paciente)));
    strcat(criteria, dni_paciente);
    // Pide más memoria para la comilla final (un caracter) y lo concatena
    criteria = (char *) realloc(criteria, (sizeof(criteria) + (sizeof(char))));
    strcat(criteria, "'");

    CLRSCR();

    // Genera el listado de todas las dietas que ha realizado ese paciente.
    tamanio_lista_dietapac = dietapac->findAll(dietapac, &lista_dieta_paciente, criteria);
    if (tamanio_lista_dietapac == 0){
        printf("Tabla vacía! No se listará nada...");
        free(lista_dieta_paciente);
        return 0;
    }

    resp = 0;
    do {
        printf("Ingrese 1 para listar a pantalla.\n");
        printf("Ingrese 2 para guardar listado en un archivo.\n");
        resp = getInt();
        if ((resp != 1)&&(resp != 2))
        printf("Ingreso inválido. Reingrese...");
    } while ((resp != 1)&&(resp != 2));

    CLRSCR();

    if (resp == 1) {
        for (int i = 0; i < tamanio_lista_dietapac; i++) {
            dietapac = ((obj_dieta_paciente**)lista_dieta_paciente)[i];

            // Busca la dieta con el código
            dieta->findbykey(dieta, dietapac->cod_dieta);

            printf("Nombre de la dieta: %s | DNI Paciente: %d | Apellido y Nombre Paciente: %s %s\n",
            dieta->nombre,
            paciente->dni,
            paciente->apellido,
            paciente->nombre);
        }
    } else {
        // Crea/Abre el archivo.
        FILE *arch;
        arch = fopen("Listado Dietas-Paciente.txt", "a+");

        fprintf(arch, CABECERA_LISTADO_DIETAS);

        for (i = 0; i < tamanio_lista_dietapac; i++) {
            dietapac = ((obj_dieta_paciente**)lista_dieta_paciente)[i];

            // Busca la dieta con el código
            dieta->findbykey(dieta, dietapac->cod_dieta);

            fprintf(arch, "\nNombre de la dieta: %s | DNI Paciente: %d | Apellido y Nombre Paciente: %s %s",
            dieta->nombre,
            paciente->dni,
            paciente->apellido,
            paciente->nombre);
        }
        printf("Finalizado!\n");
        fclose(arch);
    }
    free(lista_dieta_paciente);
}
// ----------------------------------------------------------------------------
int listarPacientes() {
    // Crea el objeto paciente y define su estructura de datos.
    obj_paciente *paciente;
    paciente = paciente_new();
    t_data_set_paciente datos_paciente;

    // Variables de uso general.
    void *lista;
    int i, tamanio;
    int resp;
    char buffer[MAX];

    // Pide el tamaño de la lista de pacientes y guarda la lista en "lista".
    tamanio = paciente->findAll(paciente, &lista, NULL);
    if (tamanio == 0){
        printf("Tabla vacía! No se listará nada...");
        return 0;
    }

    // Pide que se especifique donde se va a listar (pantalla/archivo).
    do {
        printf("Ingrese 1 para listar a pantalla.\n");
        printf("Ingrese 2 para guardar listado en un archivo.\n");
        resp = getInt();
        if ((resp != 1)&&(resp != 2))
            printf("Ingreso inválido. Reingrese...");
    } while ((resp != 1)&&(resp != 2));

    CLRSCR();

    // Guardar en archivo:
    if (resp == 2) {
        // Crea/Abre el archivo.
        FILE *arch;
        arch = fopen("Listado Pacientes.txt", "a+");

        fprintf(arch, CABECERA_LISTADO_PACIENTES);

        for (i = 0; i < tamanio; i++) {
            // Pide la dieta de posición i..
            paciente = ((obj_paciente**)lista)[i];

            fprintf(arch, "\nDNI: %d | Nombre y Apellido: %s %s | \
                    Teléfono: %s | Domicilio: %s",
                    paciente->dni,
                    paciente->nombre,
                    paciente->apellido,
                    paciente->telefono,
                    paciente->domicilio);
        }
        printf("Finalizado!\n");
        fclose(arch);
    // Listar en pantalla:
    } else {
        printf(CABECERA_LISTADO_PACIENTES);

        for (i = 0; i < tamanio; i++) {
            // Pide la dieta de posición i..
            paciente = ((obj_paciente**)lista)[i];

            printf("DNI: %d | Nombre y Apellido: %s %s | Teléfono: %s | Domicilio: %s\n",
                    paciente->dni,
                    paciente->nombre,
                    paciente->apellido,
                    paciente->telefono,
                    paciente->domicilio);
        }
    }

    // Libera la memoria usada.
    free(lista);
}
// ----------------------------------------------------------------------------
int listarIngredientes() {
    // Crea el objeto ingrediente y define su estructura de datos.
    obj_ingrediente *ingrediente;
    ingrediente = ingrediente_new();
    t_data_set_ingrediente datos_ingrediente;

    // Variables de uso general.
    void *lista;
    int i, tamanio;
    int resp;
    char buffer[MAX];

    // Pide el tamaño de la lista de ingredientes y guarda la lista en "lista".
    tamanio = ingrediente->findAll(ingrediente, &lista, NULL);
    if (tamanio == 0){
        printf("Tabla vacía! No se listará nada...");
        return 0;
    }

    // Pide que se especifique donde se va a listar (pantalla/archivo).
    do {
        printf("Ingrese 1 para listar a pantalla.\n");
        printf("Ingrese 2 para guardar listado en un archivo.\n");
        resp = getInt();
        if ((resp != 1)&&(resp != 2))
            printf("Ingreso inválido. Reingrese...\n");
    } while ((resp != 1)&&(resp != 2));

    CLRSCR();

    // Guardar en archivo:
    if (resp == 2) {
        // Crea/Abre el archivo.
        FILE *arch;
        arch = fopen("Listado Ingredientes.txt", "a+");

        fprintf(arch, CABECERA_LISTADO_INGREDIENTES);

        for (i = 0; i < tamanio; i++) {
            // Pide el ingrediente de posición i..
            ingrediente = ((obj_ingrediente**)lista)[i];
            fprintf(arch, "Codigo: %d  | Nombre: %s  | Unidad de Medida: %s\n", i + 1,
                    ingrediente->codigo,
                    ingrediente->nombre,
                    ingrediente->unidadmed);
        }
        printf("Finalizado!\n");
        fclose(arch);
    }else{    //Listar por pantalla
        printf(CABECERA_LISTADO_INGREDIENTES);

        for (i = 0; i < tamanio; i++) {
            // Pide el ingrediente de posición i..
            ingrediente = ((obj_ingrediente**)lista)[i];
            printf("Codigo: %d  | Nombre: %s  | Unidad de Medida: %s\n",
                    ingrediente->codigo,
                    ingrediente->nombre,
                    ingrediente->unidadmed);
        }
    }

    // Libera la memoria usada.
    free(lista);
}
// ----------------------------------------------------------------------------
int listarProfesionales() {
    // Crea el objeto profesional y define su estructura de datos.
    obj_profesional *profesional;
    profesional = profesional_new();

    // Variables de uso general.
    void *lista;
    int i, tamanio;
    char buffer[MAX];
    char puesto_ocupado[MAX];
    int resp, es_medico, es_nutri;

    // Pide el tamaño de la lista de profesionales y guarda la lista en "lista".
    tamanio = profesional->findAll(profesional, &lista, NULL);
    if (tamanio == 0){
        printf("Tabla vacía! No se listará nada...");
        return 0;
    }

    // Pide que se especifique donde se va a listar (pantalla/archivo).
    do {
        printf("Ingrese 1 para listar a pantalla.\n");
        printf("Ingrese 2 para guardar listado en un archivo.\n");
        resp = getInt();
        if ((resp != 1)&&(resp != 2))
            printf("Ingreso inválido. Reingrese...\n");
    } while ((resp != 1)&&(resp != 2));

    CLRSCR();

    // Guardar en archivo:
    if (resp == 2) {
        // Crea/Abre el archivo.
        FILE *arch;
        arch = fopen("Listado Profesionales.txt", "a+");

        fprintf(arch, CABECERA_LISTADO_PROFESIONALES);

        for (i = 0; i < tamanio; i++) {
            // Pide el profesional de posición i..
            profesional = ((obj_profesional**)lista)[i];
            es_medico = profesional->es_med;
            es_nutri = profesional->es_nutri;

            if ((es_medico == 1) && (es_nutri == 1)){
                strcpy(puesto_ocupado, "Médico y Nutricionista");
            } else if (es_nutri == 1){
                strcpy(puesto_ocupado, "Nutricionista");
            } else {
                strcpy(puesto_ocupado, "Médico");
            }

            fprintf(arch,"\nNombre y Apellido: %s %s | Matricula: %s | Especialidad: %s | DNI: %d | Telefono: %s",
                    profesional->nombre,
                    profesional->apellido,
                    profesional->matricula,
                    puesto_ocupado,
                    profesional->dni,
                    profesional->telefono);
        }
        printf("Finalizado!\n");
        fclose(arch);
    } else {  //Listar por pantalla
        printf(CABECERA_LISTADO_PROFESIONALES);

        for (i = 0; i < tamanio; i++) {
            // Pide el profesional de posición i..
            profesional = ((obj_profesional**)lista)[i];
            es_medico = profesional->es_med;
            es_nutri = profesional->es_nutri;

            if ((es_medico == 1) && (es_nutri == 1)){
                strcpy(puesto_ocupado, "Médico y Nutricionista");
            } else if (es_nutri == 1){
                strcpy(puesto_ocupado, "Nutricionista");
            } else {
                strcpy(puesto_ocupado, "Médico");
            }

            printf("Nombre y Apellido: %s %s | Matricula: %s | Especialidad: %s | DNI: %d | Telefono: %s\n",
                    profesional->nombre,
                    profesional->apellido,
                    profesional->matricula,
                    puesto_ocupado,
                    profesional->dni,
                    profesional->telefono);
        }

    }

    // Libera la memoria usada.
    free(lista);
}
// ----------------------------------------------------------------------------
int listarPlatosDieta() {
    // Crea el objeto plato.
    obj_plato *plato;
    plato = plato_new();
    // Crea el objeto dieta.
    obj_dieta *dieta;
    dieta = dieta_new();
    // Crea el objeto plato-dieta.
    obj_plato_dieta *pd;
    pd = plato_dieta_new();

    // Variables de uso general.
    void *lista;
    char buffer[MAX];
    char *codigo_dieta;
    char *criteria;
    int i, tamanio;
    int resp;
    int cod_dieta;
    int existe_dieta = 0;

    // Pide el codigo de la dieta y verifica que exista.
    printf("Ingrese el CODIGO de la DIETA: \n");
    codigo_dieta = getLinea();
    strcpy(buffer, codigo_dieta);
    cod_dieta = atoi(buffer);
    if (dieta->findbykey(dieta, cod_dieta) != -1) {
        existe_dieta = 1;
    } else {
        existe_dieta = 0;
        printf("La DIETA no existe. Reintente...\n");
        return 0;
    }

    CLRSCR();

    /*** Compone la cadena que va a servir de criterio de busqueda ***/
    // Pide memoria para el principio de la criteria.
    criteria = (char *) malloc(sizeof("cod_dieta='"));
    strcpy(criteria, "cod_dieta='");
    // Pide más memoria para la criteria ingresada y la concatena.
    criteria = (char *) realloc(criteria, (sizeof(criteria) + sizeof(codigo_dieta)));
    strcat(criteria, codigo_dieta);
    // Pide más memoria para la comilla final (un caracter) y lo concatena
    criteria = (char *) realloc(criteria, (sizeof(criteria) + (sizeof(char))));
    strcat(criteria, "'");

    // Genera el listado de todos los platos con ese codigo dieta.
    tamanio = pd->findAll(pd, &lista, criteria);
    if (tamanio == 0){
        printf("Tabla vacía! No se listará nada...");
        return 0;
    }

    // Sobre la salida del listado:
    resp = 0;
    do {
        printf("Ingrese 1 para listar a pantalla.\n");
        printf("Ingrese 2 para guardar listado en un archivo.\n");
        resp = getInt();
        if ((resp != 1)&&(resp != 2))
            printf("Ingreso inválido. Reingrese...\n");
    } while ((resp != 1)&&(resp != 2));

    CLRSCR();

    // Guardar en archivo:
    if (resp == 2) {
        // Crea/Abre el archivo.
        FILE *arch;
        arch = fopen("Listado Platos-Dieta.txt", "a+");

        fprintf(arch, CABECERA_LISTADO_PLATOSDIETA);

        for (i = 0; i < tamanio; i++) {
            // Pide el plato en la posición i.
            pd = ((obj_plato_dieta**)lista)[i];

            plato->findbykey(plato, pd->cod_plato);

            fprintf(arch,"\nCodigo: %d | Nombre de plato: %s | Porcion: %.2f | Nombre de dieta: %s",
                    plato->codigo,
                    plato->nombre,
                    pd->porcion,
                    dieta->nombre);
        }
        printf("Finalizado!\n");
        fclose(arch);
    } else {    // Listar en pantalla:
        printf(CABECERA_LISTADO_PLATOSDIETA);

        for (i = 0; i < tamanio; i++) {
            // Pide el plato en la posición i.
            pd = ((obj_plato_dieta**)lista)[i];

            plato->findbykey(plato, pd->cod_plato);

            printf("Codigo: %d | Nombre de plato: %s | Porcion: %.2f | Nombre de dieta: %s\n",
                    plato->codigo,
                    plato->nombre,
                    pd->porcion,
                    dieta->nombre);
        }
    }

    // Libera la memoria usada.
    free(lista);
}
// ----------------------------------------------------------------------------
int listarIngredientesPlato() {
    // Crea el objeto ingrediente.
    obj_ingrediente *ingrediente;
    ingrediente = ingrediente_new();
    // Crea el objeto plato.
    obj_plato *plato;
    plato = plato_new();
    // Crea el objeto plato-ingrediente.
    obj_plato_ingrediente *ping;
    ping = plato_ingrediente_new();

    // Variables de uso general
    void *lista;
    char buffer[MAX];
    char *codigo_plato;
    char *criteria;
    int resp, tamanio, i;
    int cod_plato;
    int existe_plato = 0;

    // Pide el codigo del plato y verifica que exista.
    printf("Ingrese el CODIGO del PLATO: \n");
    codigo_plato = getLinea();
    strcpy(buffer, codigo_plato);
    cod_plato = atoi(buffer);
    if (plato->findbykey(plato, cod_plato) != -1) {
        existe_plato = 1;
    } else {
        existe_plato = 0;
        printf("El PLATO no existe. Reintente...\n");
        return 0;
    }

    CLRSCR();

    // Sobre la salida del listado:
    resp = 0;
    do {
        printf("Ingrese 1 para listar en pantalla.\n");
        printf("Ingrese 2 para guardar listado en un archivo.\n");
        resp = getInt();
        if ((resp != 1)&&(resp != 2))
        printf("Ingreso inválido. Reingrese...");
    } while ((resp != 1)&&(resp != 2));

    /*** Compone la cadena que va a servir de criterio de busqueda ***/
    // Pide memoria para el principio de la criteria.
    criteria = (char *) malloc(sizeof("cod_plato='"));
    strcpy(criteria, "cod_plato='");
    // Pide más memoria para la criteria ingresada y la concatena.
    criteria = (char *) realloc(criteria, (sizeof(criteria) + sizeof(codigo_plato)));
    strcat(criteria, codigo_plato);
    // Pide más memoria para la comilla final (un caracter) y lo concatena
    criteria = (char *) realloc(criteria, (sizeof(criteria) + (sizeof(char))));
    strcat(criteria, "'");

    // Pide el listado y su tamaño.
    tamanio = ping->findAll(ping, &lista, criteria);
    if (tamanio == 0){
        printf("Tabla vacía! No se listará nada...");
        return 0;
    }

    CLRSCR();

    if (resp == 1){
        printf(CABECERA_LISTADO_INGREDPLATO);

        for (int i = 0; i < tamanio; i++) {
            ping = ((obj_plato_ingrediente**)lista)[i];

            ingrediente = ping->get_ingrediente(ping);

            printf("Código: %d | Nombre Ingrediente: %s | Cantidad: %.2f | Plato: %s\n",
                    ingrediente->codigo,
                    ingrediente->nombre,
                    ping->cantidad,
                    plato->nombre);
        }
    } else {
        // Crea/Abre el archivo.
        FILE *arch;
        arch = fopen("Listado Ingredientes-Plato.txt", "a+");

        fprintf(arch, CABECERA_LISTADO_INGREDPLATO);

        for (int i = 0; i < tamanio; i++) {
            ping = ((obj_plato_ingrediente**)lista)[i];

            ingrediente = ping->get_ingrediente(ping);

            fprintf(arch, "\nCódigo: %d | Nombre Ingrediente: %s | Cantidad: %.2f | Plato: %s",
                    ingrediente->codigo,
                    ingrediente->nombre,
                    ping->cantidad,
                    plato->nombre);
        }
        printf("Finalizado!\n");
        fclose(arch);
    }

    // Libera la memoria usada.
    free(lista);
}
// ----------------------------------------------------------------------------
