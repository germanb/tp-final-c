#include "config.h"
#include "utils.h"
#include "altas.h"

// ----------------------------------------------------------------------------
int altaPaciente() {
    // Crea el objeto paciente y define su estructura de datos.
    obj_paciente *paciente;
    paciente = paciente_new();
    t_data_set_paciente datos;

    // Variables de uso general.
    char buffer[MAX];
    char *temp;

    printf("\n -> Se va a dar de alta un paciente...\n");

    printf("Ingrese el DNI: ");
    temp = getLinea();
    strcpy(buffer, temp);
    datos.dni = atoi(buffer);

    // Verifica que el paciente no exista ya en la base de datos.
    if (paciente->findbykey(paciente, datos.dni) != -1) {
        printf("El paciente ingresado ya existe!\n");
        return 0;
    }

    printf("Ingrese el nombre: ");
    temp = getLinea();
    strcpy(datos.nombre, temp);

    printf("Ingrese el apellido: ");
    temp = getLinea();
    strcpy(datos.apellido, temp);

    printf("Ingrese el domicilio: ");
    temp = getLinea();
    strcpy(datos.domicilio, temp);

    printf("Ingrese el teléfono: ");
    temp = getLinea();
    strcpy(datos.telefono, temp);

    printf("Ingrese la fecha de nacimiento: ");
    temp = getLinea();
    strcpy(datos.fecha_nac, temp);

    printf("Ingrese el peso inicial: ");
    temp = getLinea();
    strcpy(buffer, temp);
    datos.peso_inicial = atof(buffer);

    printf("Ingrese la talla: ");
    temp = getLinea();
    strcpy(buffer, temp);
    datos.talla = atof(buffer);

    // Guarda el objeto en la base de datos.
    paciente->saveObj(paciente,
                        datos.dni,
                        datos.nombre,
                        datos.apellido,
                        datos.domicilio,
                        datos.telefono,
                        datos.fecha_nac,
                        datos.peso_inicial,
                        datos.talla,
                        true);

    printf("Carga finalizada...\n\n");
}
// ----------------------------------------------------------------------------
int altaDieta() {
    // Crea el objeto dieta y define su estructura de datos.
    obj_dieta *dieta;
    dieta = dieta_new();
    t_data_set_dieta datos;

    // Variables de uso general.
    char buffer[MAX];
    char *temp;

    printf("\n-> Se va a dar de alta una dieta...\n");

    printf("Ingrese el nombre de la dieta: ");
    temp = getLinea();
    strcpy(datos.nombre, temp);

    printf("Ingrese el autor de la dieta: ");
    temp = getLinea();
    strcpy(datos.autor, temp);

    printf("Ingrese una descripción para la dieta: ");
    fgets(datos.descripcion, MAX5 + 1, stdin);

    // Guarda el objeto en la base de datos.
    dieta->saveObj(dieta,
                    datos.nombre,
                    datos.autor,
                    datos.descripcion,
                    true);

    printf("Carga finalizada...\n\n");
}
// ----------------------------------------------------------------------------
int altaPlato() {
    // Crea el objeto plato y define su estructura de datos.
    obj_plato *plato;
    plato = plato_new();
    t_data_set_plato datos;

    // Variables de uso general.
    char buffer[MAX];
    char *temp;

    printf("\n-> Se va a dar de alta un plato...\n");

    printf("Ingrese el nombre: ");
    temp = getLinea();
    strcpy(datos.nombre, temp);

    printf("Ingrese un detalle: ");
    temp = getLinea();
    strcpy(datos.detalle, temp);

    // Guarda el objeto en la base de datos.
    plato->saveObj(plato,
                    datos.nombre,
                    datos.detalle,
                    true);

    printf("Carga finalizada...\n\n");
}
// ----------------------------------------------------------------------------
int altaIngrediente() {
    // Crea el objeto ingrediente y define su estructura de datos.
    obj_ingrediente *ingrediente;
    ingrediente = ingrediente_new();
    t_data_set_ingrediente datos;

    // Variables de uso general.
    char buffer[MAX];
    char *temp;

    printf("\n-> Se va a dar de alta un ingrediente...\n");

    printf("Ingrese el nombre: ");
    temp = getLinea();
    strcpy(datos.nombre, temp);

    printf("Ingrese la unidad de medida: ");
    temp = getLinea();
    strcpy(datos.unidadmed, temp);

    // Guarda el objeto en la base de datos.
    ingrediente->saveObj(ingrediente,
                         datos.nombre,
                         datos.unidadmed,
                         true);

    printf("Carga finalizada...\n\n");
}
// ----------------------------------------------------------------------------
int altaProfesional() {
    // Crea el objeto profesional y define su estructura de datos.
    obj_profesional *profesional;
    profesional = profesional_new();
    t_data_set_profesional datos;

    // Variables de uso general.
    char buffer[MAX];
    char *temp;

    printf("\n-> Se va a dar de alta un profesional...\n");

    printf("Ingrese el DNI: ");
    temp = getLinea();
    strcpy(buffer, temp);
    datos.dni = atoi(buffer);

    // Verifica que el profesional no exista ya en la base de datos.
    if (profesional->findbykey(profesional, datos.dni) != -1) {
        printf("El profesional ingresado ya existe!\n");
        return 0;
    }

    printf("Ingrese el N° de matrícula: ");
    temp = getLinea();
    strcpy(datos.matricula, temp);

    printf("Ingrese el nombre: ");
    temp = getLinea();
    strcpy(datos.nombre, temp);

    printf("Ingrese el apellido: ");
    temp = getLinea();
    strcpy(datos.apellido, temp);

    printf("Ingrese el teléfono: ");
    temp = getLinea();
    strcpy(datos.telefono, temp);

    // Valida si es médico o no.
    datos.es_med = -1;
    while ((datos.es_med != 1)&&(datos.es_med != 0)) {
        printf("Ingrese 1 si es médico (0 si no lo es): ");
        fgets(buffer, MAX + 1, stdin);
        datos.es_med = atoi(buffer);
        if ((datos.es_med != 1)&&(datos.es_med != 0))
            printf("Ingreso inválido. Reintente...\n");
    }

    // Valida si es nutricionista o no.
    datos.es_nutri = -1;
    while ((datos.es_nutri != 1)&&(datos.es_nutri != 0)) {
        printf("Ingrese 1 si es nutricionista (0 si no lo es): ");
        fgets(buffer, MAX + 1, stdin);
        datos.es_nutri = atoi(buffer);
        if ((datos.es_nutri != 1)&&(datos.es_nutri != 0))
            printf("Ingreso inválido. Reintente...\n");
    }

    // Guarda los datos en la base de datos.
    profesional->saveObj(profesional,
                         datos.dni,
                         datos.matricula,
                         datos.nombre,
                         datos.apellido,
                         datos.telefono,
                         datos.es_med,
                         datos.es_nutri,
                         true);

    printf("Carga finalizada...\n\n");
}
// ----------------------------------------------------------------------------
int altaPacienteControl() {
    // Crea el objeto paciente y define su estructura de datos.
    obj_paciente *paciente;
    paciente = paciente_new();
    t_data_set_paciente_control datos;
    // Crea el objeto paciente-control y define su estructura de datos.
    obj_paciente_control *pc;
    pc = paciente_control_new();
    t_data_set_paciente datos_paciente;

    // Variables de uso general.
    int existe_paciente;
    char buffer[MAX];
    char *temp;

    printf("\n-> Se va a agregar un control a un paciente...\n");

    // Pide el dni de un paciente y valida que exista.
    printf("Ingrese el DNI: ");
    fgets(buffer, MAX + 1, stdin);
    datos_paciente.dni = atoi(buffer);

    // Busca al paciente y valida que exista.
    if (paciente->findbykey(paciente, datos_paciente.dni) != -1){
        existe_paciente = 1;
        // Si existe devuelve los datos necesarios.
        datos.dni_paciente = datos_paciente.dni;
    } else {
        existe_paciente = 0;
        printf("El paciente no existe. Reintente...\n");
        return 0;
    }

    printf("Ingrese la fecha del control: ");
    temp = getLinea();
    strcpy(datos.fecha, temp);

    printf("Ingrese el peso del paciente: ");
    temp = getLinea();
    strcpy(buffer, temp);
    datos.peso = atof(buffer);

    // Guarda el objeto en la base.
    pc->saveObj(pc,
                datos.dni_paciente,
                datos.fecha,
                datos.peso,
                true);

    printf("Carga finalizada...\n\n");
}
// ----------------------------------------------------------------------------
int altaPacienteProfesional() {
    // Crea el objeto paciente y define su estructura de datos.
    obj_paciente *paciente;
    paciente = paciente_new();
    t_data_set_paciente datos_paciente;
    // Crea el objeto profesional y define su estructura de datos.
    obj_profesional *profesional;
    profesional = profesional_new();
    t_data_set_profesional datos_profesional;
    // Crea el objeto paciente-profesional y define su estructura de datos.
    obj_paciente_profesional *pprof;
    pprof = paciente_profesional_new();
    t_data_set_paciente_profesional datos_relacion;

    // Variables de uso general.
    int existe_paciente;
    int existe_profesional;
    char buffer[MAX];
    char *temp;

    printf("\n-> Se va a asignar un profesional a un paciente...\n");

    // Pide el DNI el paciente y valida que exista.
    printf("Ingrese el DNI del paciente: ");
    fgets(buffer, MAX + 1, stdin);
    datos_paciente.dni = atoi(buffer);
    if (paciente->findbykey(paciente, datos_paciente.dni) != -1){
        existe_paciente = 1;
        // Si existe devuelve los datos necesarios.
        datos_relacion.dni_paciente = datos_paciente.dni;
    } else {
        existe_paciente = 0;
        printf("El paciente no existe. Reintente...\n");
        return 0;
    }

    // Pide el DNI del profesional y valida que exista.
    printf("Ingrese el DNI del profesional: ");
    fgets(buffer, MAX + 1, stdin);
    datos_profesional.dni = atoi(buffer);
    if (profesional->findbykey(profesional, datos_profesional.dni) != -1){
        existe_profesional = 1;
        // Si existe devuelve los datos necesarios.
        datos_relacion.dni_medico = datos_profesional.dni;
    } else {
        existe_profesional = 0;
        printf("El profesional no existe. Reintente...\n");
        return 0;
    }

    printf("Ingrese la fecha de finalizacion: ");
    temp = getLinea();
    strcpy(datos_relacion.fecha_hasta, temp);

    // Guarda el objeto en la base de datos.
    pprof->saveObj(pprof,
                    datos_relacion.dni_paciente,
                    datos_relacion.dni_medico,
                    datos_relacion.fecha_hasta,
                    true);

    printf("Carga finalizada...\n\n");
}
// ----------------------------------------------------------------------------
int altaDietaPaciente() {
    // Crea el objeto paciente y define su estructura de datos.
    obj_paciente *paciente;
    paciente = paciente_new();
    t_data_set_paciente datos_paciente;
    // Crea el objeto dieta y define su estructura de datos.
    obj_dieta *dieta;
    dieta = dieta_new();
    t_data_set_dieta datos_dieta;
    // Crea el objeto dieta-paciente y define su estructura de datos.
    obj_dieta_paciente *dp;
    dp = dieta_paciente_new();
    t_data_set_dieta_paciente datos_dieta_paciente;

    // Variables de uso general.
    char buffer[MAX];
    char *temp;
    int existe_paciente = 0;
    int existe_dieta = 0;

    printf("\n-> Se va a asignar una dieta a un paciente...\n");

    // Pide el DNI el paciente y valida que exista.
    printf("Ingrese el DNI del paciente: ");
    fgets(buffer, MAX + 1, stdin);
    datos_paciente.dni = atoi(buffer);
    if (paciente->findbykey(paciente, datos_paciente.dni) != -1){
        existe_paciente = 1;
        // Si existe devuelve los datos necesarios.
        datos_dieta_paciente.dni_paciente = datos_paciente.dni;
    } else {
        existe_paciente = 0;
        printf("El paciente no existe. Reintente...\n");
        return 0;
    }

    // Pide el codigo de la dieta y valida que exista.
    printf("Ingrese el código de la dieta: ");
    fgets(buffer, MAX + 1, stdin);
    datos_dieta.codigo = atoi(buffer);
    if (dieta->findbykey(dieta, datos_dieta.codigo) != -1){
        existe_dieta = 1;
        // Si existe devuelve los datos necesarios.
        datos_dieta_paciente.cod_dieta = datos_dieta.codigo;
    } else {
        existe_dieta = 0;
        printf("La dieta no existe. Reintente...\n");
        return 0;
    }

    printf("Ingrese la fecha de inicio: ");
    temp = getLinea();
    strcpy(datos_dieta_paciente.fecha, temp);

    printf("Ingrese la fecha de finalizacion: ");
    temp = getLinea();
    strcpy(datos_dieta_paciente.fecha_fin, temp);

    // Guarda el objeto en la base de datos.
    dp->saveObj(dp,
                datos_dieta_paciente.cod_dieta,
                datos_dieta_paciente.dni_paciente,
                datos_dieta_paciente.fecha,
                datos_dieta_paciente.fecha_fin,
                true);

    printf("Carga finalizada...\n\n");
}
// ----------------------------------------------------------------------------
int altaPlatoIngrediente() {
    // Crea el objeto ingrediente y define su estructura de datos.
    obj_ingrediente *ingrediente;
    ingrediente = ingrediente_new();
    t_data_set_ingrediente datos_ingrediente;
    // Crea el objeto plato y define su estructura de datos.
    obj_plato *plato;
    plato = plato_new();
    t_data_set_plato datos_plato;
    // Crea el objeto plato-ingrediente y define su estructura de datos.
    obj_plato_ingrediente *ping;
    ping = plato_ingrediente_new();
    t_data_set_plato_ingrediente datos_plato_ingrediente;

    // Variables de uso general.
    char buffer[MAX];
    char *temp;
    int existe_plato = 0;
    int existe_ingrediente = 0;

    printf("\n-> Se va a agregar un ingrediente a un plato...\n");

    // Pide el codigo del plato y valida que exista.
    printf("Ingrese el código del plato: ");
    fgets(buffer, MAX + 1, stdin);
    datos_plato.codigo = atoi(buffer);
    if (plato->findbykey(plato, datos_plato.codigo) != -1){
        existe_plato = 1;
        // Si existe devuelve los datos necesarios.
        datos_plato_ingrediente.cod_plato = datos_plato.codigo;
    } else {
        existe_plato = 0;
        printf("El plato no existe. Reintente...\n");
        return 0;
    }

    // Pide el codigo del ingrediente y valida que exista.
    printf("Ingrese el código del ingrediente: ");
    fgets(buffer, MAX + 1, stdin);
    datos_ingrediente.codigo = atoi(buffer);
    if (ingrediente->findbykey(ingrediente, datos_ingrediente.codigo) != -1){
        existe_ingrediente = 1;
        // Si existe devuelve los datos necesarios.
        datos_plato_ingrediente.cod_ingrediente = datos_ingrediente.codigo;
    } else {
        existe_ingrediente = 0;
        printf("El ingrediente no existe. Reintente...\n");
        return 0;
    }

    printf("Ingrese la cantidad del ingrediente: ");
    fgets(buffer, MAX + 1, stdin);
    datos_plato_ingrediente.cantidad = atof(buffer);

    // Guarda el objeto en la base de datos.
    ping->saveObj(ping,
                    datos_plato_ingrediente.cod_plato,
                    datos_plato_ingrediente.cod_ingrediente,
                    datos_plato_ingrediente.cantidad,
                    true);

    printf("Carga finalizada...\n\n");
}
