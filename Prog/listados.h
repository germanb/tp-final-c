#include "utils.h"

#define CABECERA_LISTADO_DIETAS "--> LISTADO DE DIETAS <--\n"
#define CABECERA_LISTADO_PACIENTES "--> LISTADO DE PACIENTES <--\n"
#define CABECERA_LISTADO_INGREDIENTES "--> LISTADO DE INGREDIENTES <--\n"
#define CABECERA_LISTADO_PROFESIONALES "--> LISTADO DE PROFESIONALES <--\n"
#define CABECERA_LISTADO_PLATOSDIETA "--> LISTADO DE PLATOS (DIETA) <--\n"
#define CABECERA_LISTADO_INGREDPLATO "--> LISTADO DE INGREDIENTES (PLATO) <--\n"

// Funciones de listados.
int listarDietas(); // Verificado
int listarDietasPaciente(); // Verificado
int listarPacientes(); // Verificado
int listarIngredientes(); // Verificado
int listarProfesionales(); // Verificado
int listarPlatosDieta(); // Verificado
int listarIngredientesPlato(); // Vrificado
