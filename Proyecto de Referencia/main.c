#include <stdio.h>
#include <stdlib.h>
#include <libpq-fe.h> 
#include <string.h>
#include "config.h"
// incluir fuente de la implementacion de cada objeto del modelo
#include "orm.c"
#include "paciente.c"
#include "dieta.c"
#include "dieta_paciente.c"
#include "plato.c"
#include "plato_dieta.c"
#include "ingrediente.c"
#include "plato_ingrediente.c"
#include "plato_paciente.c"
#include "paciente_control.c"
#include "profesional.c"
#include "paciente_profesional.c"

PGconn *conn; //Instancia que permite manipular conexion con el servidor

int main(int argc, char *argv[])
{  
  char *port="5433",*servidor="localhost",*base="nutricion", *usuario="postgres", *password="master";
  obj_plato_dieta *plato_dieta;
  obj_paciente *paciente,*p;
  obj_paciente_profesional *pprof;
  obj_dieta *dieta;
  obj_dieta_paciente *dp, *ddpp ;  
  obj_plato *plato;
  obj_ingrediente *ingr;
  obj_plato_ingrediente *ping; 
  obj_plato_paciente *pp;
  obj_profesional *profesional;
  obj_paciente_control *pc;
  
  void *list; //para manejo generico de listado
  int i=0, size=0, j;
  
  connectdb(servidor,port,base,usuario,password);

  //listar todas las dietas
  dieta = dieta_new();
  size = dieta->findAll(dieta,&list,NULL); // se invoca sin criterio - listar todos...
  for(i=0;i<size;++i)
  {
    dieta = ((obj_dieta**)list)[i];
     
    printf("dieta %d): codigo : %d Nombre: %s , fecha: %s\n",i+1,dieta->codigo,dieta->nombre,dieta->fecha_alta);
  }
  free(list);
  
  printf("\n\n\n");  
  // listar todos los pacientes
  paciente = paciente_new();  
  size = paciente->findAll(paciente,&list,NULL); // se invoca sin criterio - listar todos...
  for(i=0;i<size;++i)
  {
    paciente = ((obj_paciente**)list)[i];
     
    printf("paciente %d): dni : %d Nombre: %s  Apellido: %s  - Dom: %s\n",i+1,paciente->dni,paciente->nombre,paciente->apellido,paciente->domicilio);
  }
  free(list);
  // agregar nuevo control  de un paciente dado su dni  
  pc = paciente_control_new();
  if(  pc->saveObj(pc,22445561,"2016-04-18",85.2,true) )
     printf("control de paciente agregado....\n");
  
  // buscar paciente para actualizar luego
  paciente = paciente_new();  
  if(paciente->findbykey(paciente,226322789)!=-1)
  {
	  printf("paciente %d): dni : %d Nombre: %s  Apellido: %s - Dom: %s\n",i+1,paciente->dni,paciente->nombre,paciente->apellido, paciente->domicilio);	  
	  paciente->saveObj(paciente,226322789,"Marcelo","Morales","Trelew - Edison 789" , "28045789636", "1997-02-13",82,180,false);
  }
 
  disconnectdb();
  system("PAUSE");	
  return 0;
}
